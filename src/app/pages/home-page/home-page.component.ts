import { Component, OnInit } from '@angular/core';
import axios from 'axios';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  pokemons: any[] = [];

  async ngOnInit() {

    const response = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=40&offset=0');
    this.pokemons = response.data.results;
    // console.log(this.pokemons)
    // // Equivalent :
    // axios
    // .get('https://pokeapi.co/api/v2/pokemon?limit=40&offset=0')
    // .then((response) => {
    //   this.pokemons = response.data.results;
    //   console.log(this.pokemons);
    // })
    // .catch((error) => console.log(error))
  }
}
