import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import axios from 'axios';
import { Pokemon } from 'src/app/models/pokemon';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent {

  currentPokemonId: number;
  pokemon: Pokemon | null = null;

  constructor(public route: ActivatedRoute) {
    console.log(route.snapshot.params);
    this.currentPokemonId = parseInt(route.snapshot.params['id']);
  }

  async ngOnInit() {
    const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${this.currentPokemonId}/`);
    this.pokemon = response.data;
    // console.log(this.pokemon);
  }
}
