import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent {

  @Input() pokemon: any;

  get pokemonId() {
    if (!this.pokemon) {
      return;
    }
    
    const id = parseInt(this.pokemon.url.replace('https://pokeapi.co/api/v2/pokemon/', '').replace('/', ''));

    return id
  }

  get pokemonImageUrl() {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${this.pokemonId}.svg`
  }

}
