export interface Pokemon {
    name: string;
    stats: PokemonStat[];
}

export interface APIListEntity {
    name: string;
    url: string;
}

export interface PokemonStat {
    base_stat: number;
    effort: number;
    stat: APIListEntity;
}